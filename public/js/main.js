$(document).ready(function() {
	$('#luck').click(function(evt) {
		evt.preventDefault();
		ga('send', 'event', 'button', 'action', 'getimage');
		$(".winview, #winbg, #wintoy").css({'display':'none'});
		var data = {
			w: window.screen.width,
			h: window.screen.height,
			a: window.navigator.appVersion, 
			u: window.navigator.userAgent
		}
		$.ajax({
			data: data,
			method: "POST",
			dataType: "json",
			url: "/winners",
			success: function(res) {
				console.log(res);
				$(".winview img").attr('src',res.i);
				if(res.i.indexOf("winner")<1) {
					$("#winbg,.winview").css({'display':'block'});
					$("#wintoy").css({'display':'none'});
				} else {
					$("#winbg").css({'display':'none'});
					$("#wintoy,.winview").css({'display':'block'});
				}
				var target = $("#winner");
				if (target.length) {
			        $('html,body').animate({
			          scrollTop: target.offset().top
			        }, 1000);
			        return false;
			      }
			},
			error: function(err) {
				console.log(err);
			}
		})
	});

	$('#newwin').click(function(evt) {
		evt.preventDefault();
		var img = $(".winview img").attr('src');
		ga('send', 'event', 'button', 'action', 'download', img);
		window.open(img);
	})
});