var express = require('express');
var router = express.Router();
// var bunyan = require('bunyan');
// var log = bunyan.createLogger(
// 		{
// 			name: 'wopl',
// 			streams: [{
// 				type: 'rotating-file',
// 				path: "logs/wopl.log",
// 				period: '1d',
// 				count: 5
// 			}]
// 		}
// 	);

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', 
  		{ title: 'West Orange Public Library' }
  	);
});

module.exports = router;
